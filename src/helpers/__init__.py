# external dependencies
import pandas as pd
from bs4 import BeautifulSoup
import requests

# return the elasticsearch index structure and the mapping
def get_elasticsearch_index_structure():
    return {
	    "mappings": {
            "properties": {
                "genres": { # array of strings
                    "type": "keyword",
                    # "analyzer": "keyword",
                }, 
                "director": { 
                    "type": "keyword",
                    # "analyzer": "keyword",
                },
                # we use text instead of keyword here since there are some plotwords which consist of
                # multiple words (e.g. "gay lead character" --> here, we care about the gay not the rest)
                "plotwords": { # array of strings
                    "type": "text",
                    "analyzer": "standard",
                },
                "title": { 
                    "type": "text",
                    "analyzer": "standard",
                },
                "year": { 
                    "type": "short",
                },
                "storyline": { 
                    "type": "text",
                    "analyzer": "standard",
                },
                "summary": {
                    "type": "text",
                    "analyzer": "standard",
                },
            }
        }
	}

# takes a list of Movies and converts it to the format elasticsearch requires for a bulk insert
def convert_movie_list_to_bulk(es_index, movie_list):
    try: 
        print ("\n\n\n ----------------------------------------------------------")
        print ("Amount to be added to elastic search: " + str(len(movie_list)))

        for movie in movie_list:
            print ("\n Title: " + movie.get_title())
            yield {
                "_index": es_index,
                # "doc": { 
                    "genres": movie.get_genres(),
                    "summary": movie.get_summary_text(),
                    "storyline": movie.get_storyline(),
                    "year": movie.get_year(),
                    "title": movie.get_title(),
                    "director": movie.get_director(),
                    "plotwords": movie.get_plot_keywords(),
                # },
            }

        print ("\n\n\n ----------------------------------------------------------")
    except:
        print("Movie not added to index because of incomplete data")

# read the excel file and return all Imdb Url's
def extract_urls_from_excel():
    # read the excel file with the movies
    df = pd.read_excel(r'MovieGenre.xlsx')

    # store all the IMDB url's of the Excel file
    urls = df['Imdb Link']

    return urls

# request a movie Url and extract all needed parameters using BeautifulSoup
def scrape_movie_page(movie_url):
    # extract required parameters and create a movie object
    movie = Movie()
    
    try: 
        # request the movie_url
        req = requests.get(movie_url)
        # extract the html data 
        data = req.text
        # create a Soup object with the html data
        soup = BeautifulSoup(data, features="html.parser")

        # extract the title 
        movie.set_title(soup.title.string)

        # extract the year the movie was published
        year = soup.find(id="titleYear").text if soup.find(id="titleYear") != None else ""
        # year is in format "(1995)" so the brackets need to be replaced
        year = year.replace("(", "").replace(")", "")
        movie.set_year(year)

        # extract the director
        director = soup.find_all("div", class_="credit_summary_item")[0].text if soup.find_all("div", class_="credit_summary_item")[0] != None else ""
        # director is in format "\nDirector:\nJohn Lasseter "
        director = director.replace("\nDirector:\n", "").strip()
        movie.set_director(director)

        # extract the genres
        genres = soup.find_all("div", class_="see-more inline canwrap")[1].text if soup.find_all("div", class_="see-more inline canwrap")[1] != None else ""
        # genres are in format '\nGenres:\n Animation\xa0|\n Adventure\xa0|\n Comedy\xa0|\n Family\xa0|\n Fantasy\n'
        genres = genres.replace("\nGenres:\n ", "").replace("\n", "")
        # split the genres at character "\xa0| "
        genres = genres.split("\xa0| ")
        movie.set_genres(genres)

        # extract the storyline
        storyline = soup.find_all("div", class_="inline canwrap")[0].text if soup.find_all("div", class_="inline canwrap")[0] != None else ""
        movie.set_storyline(storyline)

        # extract the plot keywords
        plot_keywords = soup.find_all("div", class_="see-more inline canwrap")[0].text if soup.find_all("div", class_="see-more inline canwrap")[0] != None else ""
        # format '\nPlot Keywords:\n toy\n|\n rivalry\n|\n cowboy\n|\n cgi animation\n|\n claw crane\n|\xa0See All (295)\xa0»\n'
        plot_keywords = plot_keywords.replace("\nPlot Keywords:\n ", "").replace("\n", "")
        # split the plot keywords at character "|"
        plot_keywords = plot_keywords.split("|")
        # the last item is a url to "see all" plot keywords and thus the last item will be deleted
        plot_keywords = plot_keywords[:-1]
        # strip all elements
        plot_keywords = [item.strip() for item in plot_keywords]
        movie.set_plot_keywords(plot_keywords)

        # extract the summary text
        summary_text = soup.find_all("div", class_="summary_text")[0].text if soup.find_all("div", class_="summary_text")[0] != None else ""
        summary_text = summary_text.replace("\n", "").strip()
        movie.set_summary_text(summary_text)

        return movie
    except KeyboardInterrupt:
        raise
    except: 
        print ("Movie data on Imdb not complete (in the scraping process). Will be skipped!")

# print the movie_data
def print_movie_data(movie):
    try: 
        print("\n Title: " + movie.get_title())
        print("\n Year: " + movie.get_year())
        print("\n Genres: ")
        print(*movie.get_genres())
        print("\n Director: " + movie.get_director())
        print("\n Storyline: " + movie.get_storyline())
        print("\n Plot Keywords: ")
        print(*movie.get_plot_keywords())
        print("\n Summary Text: " + movie.get_summary_text())

        print("------------------------- \n\n")
    except KeyboardInterrupt:
        raise
    except:
        print("Movie data not complete")

# get the elastic search query for the WWII movies
def get_es_query_wwII():
    return {
        "query": {	
            "bool": {	 	
                "must": {
                    "query_string": {
                        "query": "(world war two) | (wwii) | (world war ii)",		
                        "fields": ["plotwords", "summary"],		
                        "type": "phrase"
                        }
                    }, "filter": {
                            "range" : {						
                                "year": {			
                                    "gte": "1980",
                                    "lte": "2019"
                                    }
                                }
                            },
                            "must_not" : {
                                "query_string": {
                                    "query": "(post world war two) | (post wwii) | (post world war ii) | (after world war two) | (after wwii) | (after world war ii)",		
                                    "fields": ["plotwords", "summary"],		
                                    "type": "phrase"
                                }
                        }
                }

            },
        "_source" : ["title","summary", "plotwords", "genres"]
    }

# get the elastic search query for the directors with action movies
def get_es_query_directors():
    return {
        "query": {
            "match" : {
                "genres" : "Action"
            }
        },
        "_source": ["director"],
        "aggs" : {
            "types_count" : {       	
                "terms" :{ 
                    "field" : "director"
                } 
            }
        }
    }

# get the elastic search query for the corrupt politicians in Europe and the US
def get_es_query_corrupt_politicians():
    return {
        "min_score": 8,
        "query": {
            "query_string" : {
                        "query": "(Nixon)  | (Berlusconi) | (watergate) | (Andreotti) | (political corruption^2) | (corrupted politician) | (pentagon papers) | (government corruption^0.5) | (Pavlo Lazarenko) | (Slobodan Milosevic) | (Pavlo Lazarenko) | (crooked politician) | (political conspiracy) | (Europe^0.8) | (USA^0.8)",
                        "fields": ["plotwords", "summary", "storyline", "title"],
                        "type": "phrase"
                }
        },
        "_source": [ "title", "plotwords", "summary"]
    }

# get the elastic search query for the lgbtq movies
def get_es_query_lgbtq():
    return {
        "query": {
            "simple_query_string" : {
                "query": "(gay)  | (homosexual) | (sexuality) | (homophobia) | (lesbian) | (transgender) | (bisexual) | (queer) | (lgbt) | (lgbtq) ",
                "fields": ["plotwords"]
            }
        },
        "_source": [ "title", "plotwords", "summary"],
        "aggs" : {
            "types_count" : {       	
                "terms" :{ 
                    "field" : "year",
                    "order" : { "_count" : "asc" },
                    "size": 100
                } 
            }
        }
    }

# class to handle movie objects
class Movie:
    def __init__(self):
        pass 

    # title
    def get_title(self):
        return self.__title

    def set_title(self, title):
        self.__title = title

    # director
    def get_director(self):
        return self.__director

    def set_director(self, director):
        self.__director = director

    # genres
    def get_genres(self):
        return self.__genres

    def set_genres(self, genres):
        self.__genres = genres

    # year
    def get_year(self):
        return self.__year

    def set_year(self, year):
        self.__year = year

    # storyline
    def get_storyline(self):
        return self.__storyline

    def set_storyline(self, storyline):
        self.__storyline = storyline    

    # plot keywords
    def get_plot_keywords(self):
        return self.__plot_keywords

    def set_plot_keywords(self, plot_keywords):
        self.__plot_keywords = plot_keywords 

    # summary text
    def get_summary_text(self):
        return self.__summary_text

    def set_summary_text(self, summary_text):
        self.__summary_text = summary_text      
